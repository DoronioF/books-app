from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=100, null=True, unique=True)
    author = models.CharField(max_length=100, null=True)
    pages = models.IntegerField(null=True)
    isbn = models.IntegerField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.CharField(max_length=2000, null=True)

    def __str__(self):
        return self.title + " by " + self.author
