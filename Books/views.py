from django.shortcuts import (get_object_or_404, render, redirect, HttpResponseRedirect)
from Books.models import Book
from Books.forms import BookForm



def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, 'books/list.html', context)

def create_book(request):
    form = BookForm(request.POST or None)
    if form.is_valid():
        book = form.save()
        return redirect("show_book", pk=book.pk)
    context = {
        'form' : form
    }
    return render(request, "books/create.html", context)

def show_book(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book" : book,
    }

    return render(request, "books/detail.html", context) 

def update_view(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        form.save()
        return redirect("show_books")
    context["form"] = form

    return render(request, "books/update.html", context)

def delete_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("show_books")

    return render(request, 'books/delete.html', context)