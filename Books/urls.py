from turtle import update
from django.urls import path
from Books.views import (
    delete_book,
    show_books,
    create_book,
    show_book,
    update_view,
)
urlpatterns = [
    path("", show_books, name="show_books"),
    path("create/", create_book, name="create_books"),
    path("<int:pk>/", show_book, name="show_book"),
    path("<int:pk>/edit/", update_view, name="edit_book"),
    path("<int:pk>/delete/", delete_book, name="delete_book"),
]